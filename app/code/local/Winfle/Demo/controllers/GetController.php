<?php

	/**
	 * Class Winfle_Demo_GetController
	 *
	 * Controller for winfle/get url path.
	 * Uses GET-method handler
	 * Parameters:
	 * id -> id of product to jsonify
	 */
	class Winfle_Demo_GetController extends Mage_Core_Controller_Front_Action {

		/**
		 *	Loads layout of our site according for generated XML
		 */
		public function indexAction()
		{
			$this->loadLayout();
			$this->renderLayout();
		}

	}
