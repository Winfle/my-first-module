<?php

	/**
	 * Class Winfle_Demo_IndexController
	 *
	 * Controller for /winfle url path.
	 * The main point of our module
	 */
	class Winfle_Demo_IndexController extends Mage_Core_Controller_Front_Action {

		/**
		 *	Loads our "hello word" message
		 */
		public function indexAction()
		{
			$this->loadLayout();
			$this->renderLayout();
		}

	}