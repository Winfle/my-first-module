<?php

	/**
	 * Class Winfle_Demo_Block_Customblock
	 *
	 * This class implements all functiona
	 * llity of our Custom Block.
	 */
	class Winfle_Demo_Block_Customblock extends Mage_Core_Block_Template {
		/**
		 *
		 * This is our hero for getting JSON-string from any of product.
		 * Method is triggered by get.phtml file.
		 * Parameters:
		 * id -> id of product to jsonify
		 * @return JSON
		 */
		public function getProductJson() {
			if(!empty($getFieldsArray = $this->getRequest()->getParams()))
				if ( !is_null($requestedProduct = Mage::getModel('catalog/product')->load($getFieldsArray[ 'id' ])) )
					return json_encode($requestedProduct->getData());

			return json_encode(
				[
					'error' => 'Please input valid product ID'
				]
			);
		}

	}